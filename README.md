# C-Sharp Form Classification Program by Steve

A practice using GitLab for a Form Classification Program in C#.
----

## References on Setting Up EmguCV under C-Sharp Environment

[Image Processing in C# Basic Program](https://www.youtube.com/watch?v=8aSQR7EhNk0&t=79s)  
[OpenCV 3 Windows 10 Installation Tutorial - Part 3 - Visual Basic.NET and C# with Emgu CV](https://www.youtube.com/watch?v=7iyfJ-YaKvw&t=497s)


## File List

```
.:

./img

frmMain.cs

frmMain.Designer.cs

With some other EmguCV opencv DLLs.

```
```
/img:

MatchFormGUI.PNG

```
