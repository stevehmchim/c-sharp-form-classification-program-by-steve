﻿namespace ImageProcessing
{
    partial class frmMain
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.DataVisualization.Charting.ChartArea chartArea13 = new System.Windows.Forms.DataVisualization.Charting.ChartArea();
            System.Windows.Forms.DataVisualization.Charting.Legend legend13 = new System.Windows.Forms.DataVisualization.Charting.Legend();
            System.Windows.Forms.DataVisualization.Charting.Series series13 = new System.Windows.Forms.DataVisualization.Charting.Series();
            System.Windows.Forms.DataVisualization.Charting.ChartArea chartArea14 = new System.Windows.Forms.DataVisualization.Charting.ChartArea();
            System.Windows.Forms.DataVisualization.Charting.Legend legend14 = new System.Windows.Forms.DataVisualization.Charting.Legend();
            System.Windows.Forms.DataVisualization.Charting.Series series14 = new System.Windows.Forms.DataVisualization.Charting.Series();
            this.ofdOpenFile = new System.Windows.Forms.OpenFileDialog();
            this.btnInputOpenFile = new System.Windows.Forms.Button();
            this.ibInputOriginal = new Emgu.CV.UI.ImageBox();
            this.ibInputFiltered = new Emgu.CV.UI.ImageBox();
            this.chart1 = new System.Windows.Forms.DataVisualization.Charting.Chart();
            this.lblInputChosenFile = new System.Windows.Forms.Label();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.rbtnForm3 = new System.Windows.Forms.RadioButton();
            this.rbtnForm2 = new System.Windows.Forms.RadioButton();
            this.rbtnForm1 = new System.Windows.Forms.RadioButton();
            this.ibBlankFormOriginal = new Emgu.CV.UI.ImageBox();
            this.ibBlankFormFiltered = new Emgu.CV.UI.ImageBox();
            this.chart2 = new System.Windows.Forms.DataVisualization.Charting.Chart();
            this.lblSimilarity = new System.Windows.Forms.Label();
            this.dataGrid1 = new System.Windows.Forms.DataGrid();
            ((System.ComponentModel.ISupportInitialize)(this.ibInputOriginal)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ibInputFiltered)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chart1)).BeginInit();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ibBlankFormOriginal)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ibBlankFormFiltered)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chart2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGrid1)).BeginInit();
            this.SuspendLayout();
            // 
            // ofdOpenFile
            // 
            this.ofdOpenFile.FileName = "openFileDialog1";
            // 
            // btnInputOpenFile
            // 
            this.btnInputOpenFile.Font = new System.Drawing.Font("Calibri", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnInputOpenFile.Location = new System.Drawing.Point(24, 16);
            this.btnInputOpenFile.Name = "btnInputOpenFile";
            this.btnInputOpenFile.Size = new System.Drawing.Size(112, 48);
            this.btnInputOpenFile.TabIndex = 0;
            this.btnInputOpenFile.Text = "Open File";
            this.btnInputOpenFile.UseVisualStyleBackColor = true;
            this.btnInputOpenFile.Click += new System.EventHandler(this.btnInputOpenFile_Click);
            // 
            // ibInputOriginal
            // 
            this.ibInputOriginal.Location = new System.Drawing.Point(24, 80);
            this.ibInputOriginal.Name = "ibInputOriginal";
            this.ibInputOriginal.Size = new System.Drawing.Size(280, 360);
            this.ibInputOriginal.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.ibInputOriginal.TabIndex = 2;
            this.ibInputOriginal.TabStop = false;
            // 
            // ibInputFiltered
            // 
            this.ibInputFiltered.Location = new System.Drawing.Point(360, 80);
            this.ibInputFiltered.Name = "ibInputFiltered";
            this.ibInputFiltered.Size = new System.Drawing.Size(280, 360);
            this.ibInputFiltered.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.ibInputFiltered.TabIndex = 3;
            this.ibInputFiltered.TabStop = false;
            // 
            // chart1
            // 
            chartArea13.Name = "ChartArea1";
            this.chart1.ChartAreas.Add(chartArea13);
            legend13.Name = "Legend1";
            this.chart1.Legends.Add(legend13);
            this.chart1.Location = new System.Drawing.Point(696, 80);
            this.chart1.Name = "chart1";
            series13.ChartArea = "ChartArea1";
            series13.Color = System.Drawing.Color.MediumPurple;
            series13.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            series13.Legend = "Legend1";
            series13.Name = "Input Form";
            this.chart1.Series.Add(series13);
            this.chart1.Size = new System.Drawing.Size(480, 360);
            this.chart1.TabIndex = 4;
            this.chart1.Text = "chart1";
            // 
            // lblInputChosenFile
            // 
            this.lblInputChosenFile.AutoSize = true;
            this.lblInputChosenFile.Font = new System.Drawing.Font("Calibri", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblInputChosenFile.Location = new System.Drawing.Point(160, 24);
            this.lblInputChosenFile.Name = "lblInputChosenFile";
            this.lblInputChosenFile.Size = new System.Drawing.Size(61, 24);
            this.lblInputChosenFile.TabIndex = 5;
            this.lblInputChosenFile.Text = "label1";
            this.lblInputChosenFile.Visible = false;
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.rbtnForm3);
            this.groupBox1.Controls.Add(this.rbtnForm2);
            this.groupBox1.Controls.Add(this.rbtnForm1);
            this.groupBox1.Font = new System.Drawing.Font("Calibri", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox1.Location = new System.Drawing.Point(24, 448);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(648, 80);
            this.groupBox1.TabIndex = 6;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Blank Form Selection";
            // 
            // rbtnForm3
            // 
            this.rbtnForm3.AutoSize = true;
            this.rbtnForm3.Font = new System.Drawing.Font("Calibri", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.rbtnForm3.Location = new System.Drawing.Point(384, 32);
            this.rbtnForm3.Margin = new System.Windows.Forms.Padding(4);
            this.rbtnForm3.Name = "rbtnForm3";
            this.rbtnForm3.Size = new System.Drawing.Size(191, 28);
            this.rbtnForm3.TabIndex = 18;
            this.rbtnForm3.TabStop = true;
            this.rbtnForm3.Text = "學生課業評估報告";
            this.rbtnForm3.UseVisualStyleBackColor = true;
            this.rbtnForm3.CheckedChanged += new System.EventHandler(this.rbtnForm3_CheckedChanged);
            // 
            // rbtnForm2
            // 
            this.rbtnForm2.AutoSize = true;
            this.rbtnForm2.Font = new System.Drawing.Font("Calibri", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.rbtnForm2.Location = new System.Drawing.Point(208, 32);
            this.rbtnForm2.Margin = new System.Windows.Forms.Padding(4);
            this.rbtnForm2.Name = "rbtnForm2";
            this.rbtnForm2.Size = new System.Drawing.Size(151, 28);
            this.rbtnForm2.TabIndex = 17;
            this.rbtnForm2.TabStop = true;
            this.rbtnForm2.Text = "學生資料表格";
            this.rbtnForm2.UseVisualStyleBackColor = true;
            this.rbtnForm2.CheckedChanged += new System.EventHandler(this.rbtnForm2_CheckedChanged);
            // 
            // rbtnForm1
            // 
            this.rbtnForm1.AutoSize = true;
            this.rbtnForm1.Checked = true;
            this.rbtnForm1.Font = new System.Drawing.Font("Calibri", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.rbtnForm1.Location = new System.Drawing.Point(16, 32);
            this.rbtnForm1.Margin = new System.Windows.Forms.Padding(4);
            this.rbtnForm1.Name = "rbtnForm1";
            this.rbtnForm1.Size = new System.Drawing.Size(171, 28);
            this.rbtnForm1.TabIndex = 16;
            this.rbtnForm1.TabStop = true;
            this.rbtnForm1.Text = "教職員評核報告";
            this.rbtnForm1.UseVisualStyleBackColor = true;
            this.rbtnForm1.CheckedChanged += new System.EventHandler(this.rbtnForm1_CheckedChanged);
            // 
            // ibBlankFormOriginal
            // 
            this.ibBlankFormOriginal.Location = new System.Drawing.Point(24, 536);
            this.ibBlankFormOriginal.Name = "ibBlankFormOriginal";
            this.ibBlankFormOriginal.Size = new System.Drawing.Size(280, 360);
            this.ibBlankFormOriginal.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.ibBlankFormOriginal.TabIndex = 7;
            this.ibBlankFormOriginal.TabStop = false;
            // 
            // ibBlankFormFiltered
            // 
            this.ibBlankFormFiltered.Location = new System.Drawing.Point(360, 536);
            this.ibBlankFormFiltered.Name = "ibBlankFormFiltered";
            this.ibBlankFormFiltered.Size = new System.Drawing.Size(280, 360);
            this.ibBlankFormFiltered.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.ibBlankFormFiltered.TabIndex = 8;
            this.ibBlankFormFiltered.TabStop = false;
            // 
            // chart2
            // 
            chartArea14.Name = "ChartArea1";
            this.chart2.ChartAreas.Add(chartArea14);
            legend14.Name = "Legend1";
            this.chart2.Legends.Add(legend14);
            this.chart2.Location = new System.Drawing.Point(696, 536);
            this.chart2.Name = "chart2";
            series14.ChartArea = "ChartArea1";
            series14.Color = System.Drawing.Color.Orange;
            series14.Legend = "Legend1";
            series14.Name = "Blank Form";
            this.chart2.Series.Add(series14);
            this.chart2.Size = new System.Drawing.Size(480, 360);
            this.chart2.TabIndex = 9;
            this.chart2.Text = "chart2";
            // 
            // lblSimilarity
            // 
            this.lblSimilarity.AutoSize = true;
            this.lblSimilarity.Font = new System.Drawing.Font("Calibri", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblSimilarity.Location = new System.Drawing.Point(1192, 456);
            this.lblSimilarity.Margin = new System.Windows.Forms.Padding(4, 10, 4, 10);
            this.lblSimilarity.Name = "lblSimilarity";
            this.lblSimilarity.Size = new System.Drawing.Size(159, 37);
            this.lblSimilarity.TabIndex = 23;
            this.lblSimilarity.Text = "Similarity &%";
            // 
            // dataGrid1
            // 
            this.dataGrid1.CaptionVisible = false;
            this.dataGrid1.DataMember = "";
            this.dataGrid1.HeaderForeColor = System.Drawing.SystemColors.ControlText;
            this.dataGrid1.Location = new System.Drawing.Point(1192, 80);
            this.dataGrid1.Name = "dataGrid1";
            this.dataGrid1.Size = new System.Drawing.Size(600, 360);
            this.dataGrid1.TabIndex = 24;
            // 
            // frmMain
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 15F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1812, 961);
            this.Controls.Add(this.dataGrid1);
            this.Controls.Add(this.lblSimilarity);
            this.Controls.Add(this.chart2);
            this.Controls.Add(this.ibBlankFormFiltered);
            this.Controls.Add(this.ibBlankFormOriginal);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.lblInputChosenFile);
            this.Controls.Add(this.chart1);
            this.Controls.Add(this.ibInputFiltered);
            this.Controls.Add(this.ibInputOriginal);
            this.Controls.Add(this.btnInputOpenFile);
            this.Name = "frmMain";
            this.Text = "ImageProcessing";
            ((System.ComponentModel.ISupportInitialize)(this.ibInputOriginal)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ibInputFiltered)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chart1)).EndInit();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ibBlankFormOriginal)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ibBlankFormFiltered)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chart2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGrid1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.OpenFileDialog ofdOpenFile;
        private Emgu.CV.UI.ImageBox ibInputOriginal;
        private Emgu.CV.UI.ImageBox ibInputFiltered;
        private System.Windows.Forms.DataVisualization.Charting.Chart chart1;
        private System.Windows.Forms.Label lblInputChosenFile;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.RadioButton rbtnForm3;
        private System.Windows.Forms.RadioButton rbtnForm2;
        private System.Windows.Forms.RadioButton rbtnForm1;
        private Emgu.CV.UI.ImageBox ibBlankFormOriginal;
        private Emgu.CV.UI.ImageBox ibBlankFormFiltered;
        private System.Windows.Forms.DataVisualization.Charting.Chart chart2;
        private System.Windows.Forms.Label lblSimilarity;
        private System.Windows.Forms.Button btnInputOpenFile;
        private System.Windows.Forms.DataGrid dataGrid1;
    }
}

