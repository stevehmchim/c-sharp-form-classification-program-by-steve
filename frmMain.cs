﻿using System;
using System.IO;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

using Emgu.CV;                  //
using Emgu.CV.CvEnum;           // usual Emgu Cv imports
using Emgu.CV.Structure;        //
using Emgu.CV.UI;               //

// btnInputOpenFile (Button)
// lblInputChosenFile (Label)
// ibInputOriginal (Emgu ImageBox)
// ibInputFiltered (Emgu ImageBox)

// ibBlankFormOriginal (Emgu ImageBox)
// ibBlankFormFiltered (Emgu ImageBox)

// ofdOpenFile (OpenFileDialog)




namespace ImageProcessing
{
    public partial class frmMain : Form
    {
        const int dim = 10; // For a 10 x 10 array now
        const int diffHistArrayThres = 100;
        int[,] inputFormHistArray = new int[dim, dim];
        int[,] blankFormHistArray = new int[dim, dim];
        int[,] histDiffArray = new int[dim, dim];
        bool bFileOpened = false;
        bool bBlankFormChosen = false;


        public frmMain()
        {
            InitializeComponent();

            /// <summary>
            /// 
            ///                     Binding a two dimensional array to a DataGrid
            ///                     https://www.codeproject.com/Articles/5806/Binding-a-two-dimensional-array-to-a-DataGrid
            ///                     **NEED TO CHANGE References PATH Mommo.Data TO CURRENT PROJECT FOLDER**
            /// 
            /// </summary>
      

            //const int dim = dim00;

            //// can use any basic type.
            //double[,] array = new double[dim, dim];

            //Random ran = new Random();
            //for (int r = 0; r < dim; r++)
            //{
            //    for (int c = 0; c < dim; c++)
            //    {
            //        array[r, c] = (ran.Next(dim));
            //    }
            //}
            //string[] colnames = new string[dim];
            //for (int c = 0; c < dim; c++)
            //{
            //    colnames[c] = string.Format("-{0}-", c);
            //}
            ////dataGrid1.DataSource = new Mommo.Data.ArrayDataView(array);
            //dataGrid1.DataSource = new Mommo.Data.ArrayDataView(array, colnames);
        }

        private void find_Similarity()
        {
            int counter = 0;

            for (int r = 0; r < dim; r++)
            {
                for (int c = 0; c < dim; c++)
                {
                    if (histDiffArray[c, r] < diffHistArrayThres)
                        counter++;
                }
            }
            string strCoordinates = string.Format("Similiarity: {0}&%", counter);
            lblSimilarity.Text = strCoordinates;
        }


        private void calculateHistDiff()
        {
            for (int r = 0; r < dim; r++)
            {
                for (int c = 0; c < dim; c++)
                {
                    int tempDiff = inputFormHistArray[r, c] - blankFormHistArray[r, c];
                    histDiffArray[c, r] = Math.Abs(tempDiff);
                }
            }
        }


        private void showHistDiffDataGrid()
        {
            string[] colnames = new string[dim];
            for (int c = 0; c < dim; c++)
            {
                colnames[c] = string.Format("-{0}-", c);
            }
            //dataGrid1.DataSource = new Mommo.Data.ArrayDataView(array);
            dataGrid1.DataSource = new Mommo.Data.ArrayDataView(histDiffArray, colnames);
        }






        private void btnInputOpenFile_Click(object sender, EventArgs e)
        {
            DialogResult drChosenFile;

            drChosenFile = ofdOpenFile.ShowDialog();

            if (drChosenFile != DialogResult.OK || ofdOpenFile.FileName == "")
            {
                lblInputChosenFile.Text = "file not chosen";
                return;
            }

            Mat inputFormColor;

            try
            {
                inputFormColor = new Mat(ofdOpenFile.FileName, ImreadModes.Color);
            }
            catch (Exception ex)
            {
                lblInputChosenFile.Text = "unable to open image, error: " + ex.Message;
                return;
            }

            if (inputFormColor == null)
            {
                lblInputChosenFile.Text = "unable to open image";
                return;
            }

            bFileOpened = true;

            CvInvoke.Resize(inputFormColor, inputFormColor, new Size(0, 0), 0.25, 0.25, Inter.Cubic);

            Mat inputFormGray = new Mat(inputFormColor.Size, DepthType.Cv8U, 1);
            Mat inputFormGrayFiltered = new Mat(inputFormColor.Size, DepthType.Cv8U, 1);
            Mat inputFormComplement = new Mat(inputFormColor.Size, DepthType.Cv8U, 1);

            inputFormComplement.SetTo(new MCvScalar(255));

            CvInvoke.CvtColor(inputFormColor, inputFormGray, ColorConversion.Bgr2Gray);

            CvInvoke.BilateralFilter(inputFormGray, inputFormGrayFiltered, 9, 80.0, 80.0);

            CvInvoke.AdaptiveThreshold(inputFormGrayFiltered, inputFormGrayFiltered, 255, AdaptiveThresholdType.GaussianC, ThresholdType.Binary, 11, 2);

            CvInvoke.Subtract(inputFormComplement, inputFormGrayFiltered, inputFormComplement);

            Size inputFormSize = new Size(450, 650);

            Point inputFormCenter = new Point(inputFormGray.Size.Width / 2, inputFormGray.Size.Height / 2);

            CvInvoke.GetRectSubPix(inputFormComplement, inputFormSize, inputFormCenter, inputFormComplement);

            Size cropSize = new Size(45, 65);

            for (int i = 0; i < dim; i++)
            {
                for (int j = 0; j < dim; j++)
                {
                    int counter = 0;
                    Mat cropForm = new Mat(cropSize, DepthType.Cv8U, 1);
                    Point cropSizeCenter = new Point(45 * i + 22, 65 * j + 32);
                    CvInvoke.GetRectSubPix(inputFormComplement, cropSize, cropSizeCenter, cropForm);
                    for (int height = 0; height < cropForm.Rows; height++)
                        for (int width = 0; width < cropForm.Cols; width++)
                        {
                            if (Convert.ToInt16(cropForm.GetData(height, width)[0]) != 0)
                            {
                                counter++;
                            }
                        }
                    inputFormHistArray[i, j] = counter;
                }
            }

            //string fileName = System.IO.Path.GetFileNameWithoutExtension(ofdOpenFile.FileName);
            //string arrayFileName = "C:\\CsharpTemp\\" +  fileName + "_Array.txt";
            //using (StreamWriter sw = new StreamWriter(arrayFileName))
            //{
            //    //sw.WriteLine("counterArray");
            //    for (int i = 0; i < dim; i++)
            //    {
            //        for (int j = 0; j < dim; j++)
            //        {
            //            sw.Write(histogram[j, i] + " ");
            //        }
            //        sw.WriteLine("");
            //    }

            //    sw.Flush();
            //    sw.Close();
            //}

            ibInputFiltered.Image = inputFormComplement;
            ibInputOriginal.Image = inputFormColor;

            chart1.Series["Input Form"].Points.Clear();
            chart1.ChartAreas[0].AxisY.Maximum = 1500;

            for (int i = 0; i < dim; i++)
            {
                for (int j = 0; j < dim; j++)
                {
                    chart1.Series["Input Form"].Points.Add(inputFormHistArray[i, j]);
                }
            }

            if (bFileOpened && bBlankFormChosen)
            {
                calculateHistDiff();
                showHistDiffDataGrid();
                find_Similarity();
            }
        }

        private void rbtnForm1_CheckedChanged(object sender, EventArgs e)
        {
            string blankFormFileName = "C:\\CsharpTemp\\Test_images_Blank_forms\\BlankForms\\BlankForm001教職員評核報告.jpg";

            bBlankFormChosen = true;

            Mat blankFormColor = new Mat(blankFormFileName, ImreadModes.Color);

            CvInvoke.Resize(blankFormColor, blankFormColor, new Size(0, 0), 0.25, 0.25, Inter.Cubic);

            Mat blankFormGray = new Mat(blankFormColor.Size, DepthType.Cv8U, 1);
            Mat blankFormGrayFiltered = new Mat(blankFormColor.Size, DepthType.Cv8U, 1);
            Mat blankFormComplement = new Mat(blankFormColor.Size, DepthType.Cv8U, 1);

            blankFormComplement.SetTo(new MCvScalar(255));

            CvInvoke.CvtColor(blankFormColor, blankFormGray, ColorConversion.Bgr2Gray);

            CvInvoke.BilateralFilter(blankFormGray, blankFormGrayFiltered, 9, 80.0, 80.0);

            CvInvoke.AdaptiveThreshold(blankFormGrayFiltered, blankFormGrayFiltered, 255, AdaptiveThresholdType.GaussianC, ThresholdType.Binary, 11, 2);

            CvInvoke.Subtract(blankFormComplement, blankFormGrayFiltered, blankFormComplement);

            Size blankFormSize = new Size(450, 650);

            Point blankFormCenter = new Point(blankFormGray.Size.Width / 2, blankFormGray.Size.Height / 2);

            CvInvoke.GetRectSubPix(blankFormComplement, blankFormSize, blankFormCenter, blankFormComplement);

            Size cropSize = new Size(45, 65);

            for (int i = 0; i < dim; i++)
            {
                for (int j = 0; j < dim; j++)
                {
                    int counter = 0;
                    Mat cropForm = new Mat(cropSize, DepthType.Cv8U, 1);
                    Point cropSizeCenter = new Point(45 * i + 22, 65 * j + 32);
                    CvInvoke.GetRectSubPix(blankFormComplement, cropSize, cropSizeCenter, cropForm);
                    for (int height = 0; height < cropForm.Rows; height++)
                        for (int width = 0; width < cropForm.Cols; width++)
                        {
                            if (Convert.ToInt16(cropForm.GetData(height, width)[0]) != 0)
                            {
                                counter++;
                            }
                        }
                    blankFormHistArray[i, j] = counter;
                }
            }

            //string fileName = System.IO.Path.GetFileNameWithoutExtension(ofdOpenFile.FileName);
            //string arrayFileName = "C:\\CsharpTemp\\" +  fileName + "_Array.txt";
            //using (StreamWriter sw = new StreamWriter(arrayFileName))
            //{
            //    //sw.WriteLine("counterArray");
            //    for (int i = 0; i < dim; i++)
            //    {
            //        for (int j = 0; j < dim; j++)
            //        {
            //            sw.Write(histogram[j, i] + " ");
            //        }
            //        sw.WriteLine("");
            //    }

            //    sw.Flush();
            //    sw.Close();
            //}

            ibBlankFormOriginal.Image = blankFormColor;
            ibBlankFormFiltered.Image = blankFormComplement;

            chart2.Series["Blank Form"].Points.Clear();
            chart2.ChartAreas[0].AxisY.Maximum = 1500;

            for (int i = 0; i < dim; i++)
            {
                for (int j = 0; j < dim; j++)
                {
                    chart2.Series["Blank Form"].Points.Add(blankFormHistArray[i, j]);
                }
            }

            if (bFileOpened && bBlankFormChosen)
            {
                calculateHistDiff();
                showHistDiffDataGrid();
                find_Similarity();
            }
        }

        private void rbtnForm2_CheckedChanged(object sender, EventArgs e)
        {
            string blankFormFileName = "C:\\CsharpTemp\\Test_images_Blank_forms\\BlankForms\\BlankForm002學生資料表格.jpg";

            bBlankFormChosen = true;

            Mat blankFormColor = new Mat(blankFormFileName, ImreadModes.Color);

            CvInvoke.Resize(blankFormColor, blankFormColor, new Size(0, 0), 0.25, 0.25, Inter.Cubic);

            Mat blankFormGray = new Mat(blankFormColor.Size, DepthType.Cv8U, 1);
            Mat blankFormGrayFiltered = new Mat(blankFormColor.Size, DepthType.Cv8U, 1);
            Mat blankFormComplement = new Mat(blankFormColor.Size, DepthType.Cv8U, 1);

            blankFormComplement.SetTo(new MCvScalar(255));

            CvInvoke.CvtColor(blankFormColor, blankFormGray, ColorConversion.Bgr2Gray);

            CvInvoke.BilateralFilter(blankFormGray, blankFormGrayFiltered, 9, 80.0, 80.0);

            CvInvoke.AdaptiveThreshold(blankFormGrayFiltered, blankFormGrayFiltered, 255, AdaptiveThresholdType.GaussianC, ThresholdType.Binary, 11, 2);

            CvInvoke.Subtract(blankFormComplement, blankFormGrayFiltered, blankFormComplement);

            Size blankFormSize = new Size(450, 650);

            Point blankFormCenter = new Point(blankFormGray.Size.Width / 2, blankFormGray.Size.Height / 2);

            CvInvoke.GetRectSubPix(blankFormComplement, blankFormSize, blankFormCenter, blankFormComplement);

            Size cropSize = new Size(45, 65);

            for (int i = 0; i < dim; i++)
            {
                for (int j = 0; j < dim; j++)
                {
                    int counter = 0;
                    Mat cropForm = new Mat(cropSize, DepthType.Cv8U, 1);
                    Point cropSizeCenter = new Point(45 * i + 22, 65 * j + 32);
                    CvInvoke.GetRectSubPix(blankFormComplement, cropSize, cropSizeCenter, cropForm);
                    for (int height = 0; height < cropForm.Rows; height++)
                        for (int width = 0; width < cropForm.Cols; width++)
                        {
                            if (Convert.ToInt16(cropForm.GetData(height, width)[0]) != 0)
                            {
                                counter++;
                            }
                        }
                    blankFormHistArray[i, j] = counter;
                }
            }

            //string fileName = System.IO.Path.GetFileNameWithoutExtension(ofdOpenFile.FileName);
            //string arrayFileName = "C:\\CsharpTemp\\" +  fileName + "_Array.txt";
            //using (StreamWriter sw = new StreamWriter(arrayFileName))
            //{
            //    //sw.WriteLine("counterArray");
            //    for (int i = 0; i < dim; i++)
            //    {
            //        for (int j = 0; j < dim; j++)
            //        {
            //            sw.Write(histogram[j, i] + " ");
            //        }
            //        sw.WriteLine("");
            //    }

            //    sw.Flush();
            //    sw.Close();
            //}

            ibBlankFormOriginal.Image = blankFormColor;
            ibBlankFormFiltered.Image = blankFormComplement;

            chart2.Series["Blank Form"].Points.Clear();
            chart2.ChartAreas[0].AxisY.Maximum = 1500;

            for (int i = 0; i < dim; i++)
            {
                for (int j = 0; j < dim; j++)
                {
                    chart2.Series["Blank Form"].Points.Add(blankFormHistArray[i, j]);
                }
            }
            if (bFileOpened && bBlankFormChosen)
            {
                calculateHistDiff();
                showHistDiffDataGrid();
                find_Similarity();
            }
        }

        private void rbtnForm3_CheckedChanged(object sender, EventArgs e)
        {
            string blankFormFileName = "C:\\CsharpTemp\\Test_images_Blank_forms\\BlankForms\\BlankForm003學生課業評估報告.jpg";

            bBlankFormChosen = true;

            Mat blankFormColor = new Mat(blankFormFileName, ImreadModes.Color);

            CvInvoke.Resize(blankFormColor, blankFormColor, new Size(0, 0), 0.25, 0.25, Inter.Cubic);

            Mat blankFormGray = new Mat(blankFormColor.Size, DepthType.Cv8U, 1);
            Mat blankFormGrayFiltered = new Mat(blankFormColor.Size, DepthType.Cv8U, 1);
            Mat blankFormComplement = new Mat(blankFormColor.Size, DepthType.Cv8U, 1);

            blankFormComplement.SetTo(new MCvScalar(255));

            CvInvoke.CvtColor(blankFormColor, blankFormGray, ColorConversion.Bgr2Gray);

            CvInvoke.BilateralFilter(blankFormGray, blankFormGrayFiltered, 9, 80.0, 80.0);

            CvInvoke.AdaptiveThreshold(blankFormGrayFiltered, blankFormGrayFiltered, 255, AdaptiveThresholdType.GaussianC, ThresholdType.Binary, 11, 2);

            CvInvoke.Subtract(blankFormComplement, blankFormGrayFiltered, blankFormComplement);

            Size blankFormSize = new Size(450, 650);

            Point blankFormCenter = new Point(blankFormGray.Size.Width / 2, blankFormGray.Size.Height / 2);

            CvInvoke.GetRectSubPix(blankFormComplement, blankFormSize, blankFormCenter, blankFormComplement);

            Size cropSize = new Size(45, 65);

            for (int i = 0; i < dim; i++)
            {
                for (int j = 0; j < dim; j++)
                {
                    int counter = 0;
                    Mat cropForm = new Mat(cropSize, DepthType.Cv8U, 1);
                    Point cropSizeCenter = new Point(45 * i + 22, 65 * j + 32);
                    CvInvoke.GetRectSubPix(blankFormComplement, cropSize, cropSizeCenter, cropForm);
                    for (int height = 0; height < cropForm.Rows; height++)
                        for (int width = 0; width < cropForm.Cols; width++)
                        {
                            if (Convert.ToInt16(cropForm.GetData(height, width)[0]) != 0)
                            {
                                counter++;
                            }
                        }
                    blankFormHistArray[i, j] = counter;
                }
            }

            //string fileName = System.IO.Path.GetFileNameWithoutExtension(ofdOpenFile.FileName);
            //string arrayFileName = "C:\\CsharpTemp\\" +  fileName + "_Array.txt";
            //using (StreamWriter sw = new StreamWriter(arrayFileName))
            //{
            //    //sw.WriteLine("counterArray");
            //    for (int i = 0; i < dim; i++)
            //    {
            //        for (int j = 0; j < dim; j++)
            //        {
            //            sw.Write(histogram[j, i] + " ");
            //        }
            //        sw.WriteLine("");
            //    }

            //    sw.Flush();
            //    sw.Close();
            //}

            ibBlankFormOriginal.Image = blankFormColor;
            ibBlankFormFiltered.Image = blankFormComplement;

            chart2.Series["Blank Form"].Points.Clear();
            chart2.ChartAreas[0].AxisY.Maximum = 1500;

            for (int i = 0; i < dim; i++)
            {
                for (int j = 0; j < dim; j++)
                {
                    chart2.Series["Blank Form"].Points.Add(blankFormHistArray[i, j]);
                }
            }
            if (bFileOpened && bBlankFormChosen)
            {
                calculateHistDiff();
                showHistDiffDataGrid();
                find_Similarity();
            }
        }


    }
}
